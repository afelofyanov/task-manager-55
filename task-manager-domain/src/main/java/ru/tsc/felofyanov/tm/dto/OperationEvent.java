package ru.tsc.felofyanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.EntityOperationType;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @NotNull
    private EntityOperationType operationType;

    @NotNull
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull EntityOperationType operationType, @NotNull Object entity) {
        this.operationType = operationType;
        this.entity = entity;
    }
}
