package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.api.service.ILoggerService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.ITokenService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.felofyanov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.felofyanov.tm.util.SystemUtil;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
public final class Bootstrap {

    private static final String PACKAGE_COMMANDS = "ru.tsc.felofyanov.tm.command";

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        registry(abstractCommands);
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SHUTTING DOWN**");
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();

        while (true) processCommand();
    }

    private void registry(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.err.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }
}
