package ru.tsc.felofyanov.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final List<String> commands = new ArrayList<>();

    private final File folder = new File("./tmp");

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    private void init() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));

        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        init();
    }
}
