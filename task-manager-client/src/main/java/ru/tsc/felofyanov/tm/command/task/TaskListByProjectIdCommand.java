package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.request.TaskListByProjectIdRequest;
import ru.tsc.felofyanov.tm.dto.response.TaskListByProjectIdResponse;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskListByProjectIdRequest request =
                new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();

        renderTasks(tasks);
    }
}
