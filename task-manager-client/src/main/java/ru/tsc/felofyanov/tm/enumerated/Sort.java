package ru.tsc.felofyanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Sort {

    NAME("Sort by name"),
    STATUS("Sort by status"),
    CREATED("Sort by created"),
    START_DT("Sort by date begin");

    @Getter
    @NotNull
    private final String displayName;

    Sort(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values())
            if (sort.name().equals(value)) return sort;
        return null;
    }
}