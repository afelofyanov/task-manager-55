package ru.tsc.felofyanov.tm.command.taskproject;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class BindTaskToProjectCommand extends AbstractTaskProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "bind-task-to-project";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project.";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectBindTaskByIdRequest request =
                new ProjectBindTaskByIdRequest(getToken(), projectId, taskId);
        getProjectTaskEndpoint().bindTaskToProjectId(request);
    }
}
