package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);

        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, status);
        getProjectEndpoint().changeProjectStatusById(request);
    }
}
