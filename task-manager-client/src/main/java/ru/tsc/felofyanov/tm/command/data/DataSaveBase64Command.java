package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataSaveBase64Request;
import ru.tsc.felofyanov.tm.enumerated.Role;

@Component
public final class DataSaveBase64Command extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in base64 file";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataBase64(new DataSaveBase64Request(getToken()));
    }
}
