package ru.tsc.felofyanov.tm.command.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;
}
