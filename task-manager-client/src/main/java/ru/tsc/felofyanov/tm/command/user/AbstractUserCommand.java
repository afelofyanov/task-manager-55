package ru.tsc.felofyanov.tm.command.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;
}
