package ru.tsc.felofyanov.tm.api.service.model;

import ru.tsc.felofyanov.tm.api.repository.model.IRepository;
import ru.tsc.felofyanov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
