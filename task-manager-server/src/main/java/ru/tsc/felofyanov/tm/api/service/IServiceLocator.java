package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();
}
