package ru.tsc.afelofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.service.model.ITaskService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.service.model.TaskService;

import java.util.List;

public class TaskServiceTest extends AbstractTest {

    @NotNull
    private ITaskService taskService;

    @Before
    @Override
    public void init() {
        super.init();
        taskService = new TaskService(CONNECTION_SERVICE);
    }

    @After
    @Override
    public void close() {
        super.close();
    }

    @Test
    public void add() {
        @NotNull Task project = new Task(testUser, "asdasd", "ddd");
        Assert.assertTrue(taskService.findAll().isEmpty());

        @Nullable Task projectNull = null;
        Assert.assertThrows(ModelEmptyException.class, () -> taskService.add(projectNull));

        taskService.add(project);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertNotNull(taskService.findOneById(project.getId()));
    }

    @Test
    public void updateById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, "test", "test", "test"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById("test", "test", null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById("test", "test", "test", null));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.updateById("test", "test", "test", "test"));

        @Nullable Task project = taskService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(taskService.findOneById(project.getId()));

        Assert.assertNotNull(taskService.updateById(testUser.getId(), project.getId(), "No demo", "tester"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(null, 0, "test", "test"));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex("test", 0, null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateByIndex("test", 0, "test", null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex("test", -1, "test", "test"));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.updateByIndex(testUser.getId(), 1, "test", "test"));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.updateByIndex(testUser.getId(), 0, "test", "test"));

        taskService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.updateByIndex(testUser.getId(), 0, "No demo", "tester"));
    }

    @Test
    public void changeStatusById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeStatusById(null, "test", Status.NOT_STARTED));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeStatusById("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeStatusById("test", "test", null));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.changeStatusById("test", "test", Status.NOT_STARTED));

        @NotNull Task project = taskService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(taskService.findOneById(project.getId()));

        Assert.assertNotNull(taskService.changeStatusById(testUser.getId(), project.getId(), Status.IN_PROGRESS));
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeStatusByIndex(null, 0, Status.NOT_STARTED));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeStatusByIndex("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeStatusByIndex("test", 0, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeStatusByIndex("test", -1, Status.NOT_STARTED));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.changeStatusByIndex("test", 1, Status.NOT_STARTED));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.changeStatusByIndex("test", 0, Status.NOT_STARTED));

        taskService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.changeStatusByIndex(testUser.getId(), 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.create(null, ""));
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.create(null, "test"));
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.create(null, null));

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.createByUserId("", null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.createByUserId("test", null));

        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.createByUserId("test", "test", null));

        taskService.createByUserId(testUser.getId(), "test");
        Assert.assertFalse(taskService.findAll().isEmpty());
        @Nullable List<Task> serviceTest = taskService.findAllByUserId("test1");
        Assert.assertEquals(0, serviceTest.size());

        serviceTest = taskService.findAllByUserId(testUser.getId());
        Assert.assertEquals(1, serviceTest.size());
    }
}
