package ru.tsc.felofyanov.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.felofyanov.tm.listener.LoggerListener;

import javax.jms.ConnectionFactory;

@ComponentScan("ru.tsc.felofyanov.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    @NotNull
    public LoggerListener loggerListener() {
        return new LoggerListener();
    }
}
